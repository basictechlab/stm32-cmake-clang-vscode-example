# TODO add new toolchain file using arm-none-eabi-gcc
# later, make separate tasks which will use a proper toolchain, selected by a user

# set operating system, or rather lack of it
# its important, because it turns off all flags automatically added by cmake
# which on Windows system would be targeted for it
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

# set compiler and targets
set(CLANG_TARGET_TRIPLET arm-none-eabi)
set(CMAKE_C_COMPILER clang)
set(CMAKE_C_COMPILER_TARGET ${CLANG_TARGET_TRIPLET}) 
set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_COMPILER_TARGET ${CLANG_TARGET_TRIPLET})
set(CMAKE_ASM_COMPILER clang)
set(CMAKE_ASM_COMPILER_TARGET ${CLANG_TARGET_TRIPLET}) 
set(CMAKE_OBJCOPY llvm-objcopy) # zmienić na find_program i uzupełnić, że jeśli nie znaleziono, to wysyłamy krytycznego message'a
set(CMAKE_SIZE llvm-size)

# add options for cortex-m1 chip
set(CORTEXM3_OPTIONS -mcpu=cortex-m3 -mfloat-abi=soft)

# add toolchain specific compiler options
add_compile_options(
    ${CORTEXM3_OPTIONS}
    # -fmessage-length=0 # ustawia szerokość zawijania długiuch tekstów
    -funsigned-char # ustawia, że char == unsigned char
    # -ffunction-sections
    # -fdata-sections
    # -MMD
    # -MP
    # -Wl,-fuse-ld=ld
    --sysroot=C:/VSARM/armcc/arm-none-eabi
    -stdlib++-isystemC:/VSARM/armcc/arm-none-eabi/include/c++/9.3.1
    -IC:/VSARM/armcc/arm-none-eabi/include/c++/9.3.1/arm-none-eabi
)

add_compile_definitions(
    STM32F103xB # TODO potem to dopasować - pewnie powinno być to ustawiane w projekcie konkretnym
)

add_link_options(
    ${CORTEXM3_OPTIONS}
    -nostdlib # jako że krzyżokompilujemy, to podlinkowujemy biblioteki jawnie
)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
# set(CMAKE_FIND_ROOT_PATH ${COMPILER_PATH})