clang --target=arm-none-eabi -mcpu=cortex-m1 -mfloat-abi=soft -nostdlib -gdwarf-3 -Wall $INCS -DSTM32F103xB -c -v blink_led.cpp -o build2/blink_led.o


llvm-objcopy -I elf32-littlearm -O binary build/Blink_cmake.elf build/Blink_cmake.bin

dd bs=1024 count=64 if=build/Blink_cmake.bin of=build/Blink_cmake_trunc.bin


cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=toolchain-stm32f1-clang.cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

openocd --file interface/stlink.cfg --file target/stm32f1x.cfg -c "program filename.bin verify reset 
exit 0x08000000"

clang --target=arm-none-eabi -mcpu=cortex-m3 --sysroot=C:/VSARM/armcc/arm-none-eabi -LC:/VSARM/armcc/lib/gcc/arm-none-eabi/9.3.1 -IC:\VSARM\armcc\arm-none-eabi\include\c++\9.3.1\ -IC:\VSARM\armcc\arm-none-eabi\include\c++\9.3.1\arm-none-eabi -rtlib=libgcc blink_led.cpp -o ./build/xddd.o -v
 jeśli wykorzytać flagę nostdlib to trzeba podlinkować jawnie libc libstdc++ oraz libgcc (i ew. libm)
clang --target=arm-none-eabi -mcpu=cortex-m3 --sysroot=C:/VSARM/armcc/arm-none-eabi -LC:/VSARM/armcc/lib/gcc/arm-none-eabi/9.3.1 -IC:\VSARM\armcc\arm-none-eabi\include\c++\9.3.1\ -IC:\VSARM\armcc\arm-none-eabi\include\c++\9.3.1\arm-none-eabi -rtlib=libgcc blink_led.cpp -lnosys -o ./build/xddd.o -v
    wyżej z nosysem